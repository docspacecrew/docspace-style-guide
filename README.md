# DocSpace JavaScript/TypeScript Style Guide

Style guide to follow for any code written in JavaScript or TypeScript

## Table of Contents
1. [Project Structure for Backend](#backend)
2. [File Names](#file-names)
3. [Types](#types)
4. [Variables and Functions](#var-func-naming)
5. [Classes](#class-naming)
6. [Interfaces](#interface-naming)
7. [References](#references)
8. [Objects](#objects)
9. [Arrays](#arrays)
9. [Strings](#strings)

## Project Structure for Backend
Backend/API project should be structured along the following convention
```
.
├── specs/
│   └── swagger-doc.yaml
├── src/
│   ├── db-models/
│   │   └── user-table-model.ts
│   ├── factories/
│   │   └── container-factory.ts
│   ├── handlers/
│   │   └── user-handler.ts
│   ├── impl/
│   │   └── user-impl.ts
│   ├── interfaces/
│   │   ├── user-interface.ts
│   │   ├── user-impl-interface.ts
│   │   ├── logger-interface.ts
│   │   ├── http-interface.ts
│   │   └── some-service-interface.ts
│   ├── request-validators/
│   │   ├── user-api-validators.ts
│   │   └── other-route-validators.ts
│   ├── routes/
│   │   └── routes.ts
│   ├── services/
│   │   ├── axios-service.ts
│   │   ├── s3-service.ts
│   │   └── db-operations-service.ts
│   ├── utils/
│   │   ├── constants.ts
│   │   ├── error-codes.ts
│   │   ├── http-utils.ts
│   │   ├── types.ts
│   │   ├── joi-request-validators.ts
│   │   └── request-handlers.ts
│   ├── app.local.ts
│   ├── express.app.ts
│   └── lambda.ts
└── tests/
    ├── mock-services/
    │   └── s3-mock-service.ts
    └── user-tests.ts
```

## File Names
All file names need to be `kebab-case`. Please do not use `camelCase`, `snake_case`, `PascalCase` for naming the files.

```
//bad
UserService.ts
UserInterface.ts

//good
user-service.ts
user-interface.ts
```
### Why?
>You should imagine that any folder or file might be extracted to its own package some day. Packages cannot contain uppercase letters.

## Types
- Using TypeScript comes with usage of its three primitive types `string`, `number`, `boolean`. Use type annotations on variables.
```
//bad
let name = "John Doe";

//good
let name: string = "John Doe";
```

- Try to avoid using `any` as much as possible for better clarity of code.Replace usage of `any` with a properly contructed object interface in the class.
```
//bad
let user: any = {
    name: "John Smith",
    age: 30
}

let users: any[] = [
    {
        name: "John Smith",
        age: 30
    }
]

//good
let user: User = {
    name: "John Smith",
    age: 30
}

let users: User[] = [
    {
        name: "John Smith",
        age: 30
    }
]

interface User {
    name: string;
    age: number;
}
```

## Variables and Functions
- Use `camelCase` for variable and function names
```
//bad
let UserId = "1234";
private void GetUser(UserId: string) {
    ...
}

//good
let userId: string = "1234";
private void getUser(userId: string) {
    ...
}
```

- Explicitly use `public` and `private` members for functions
Make sure the function declaration is preceeded by explicit mention of its membership type - either public or private
```
//bad
getUser() {...}

or

getUser = async () => {...}

//good
public void getUser() {...}
```

- Avoid declaring anonymous functions inside a class, always use named functions
```
//bad
let myAdd = function (x, y) {
  return x + y;
};

//good
public add(x, y): number {
  return x + y;
}
```

- Strict usage of appropriate function return Type
If your function is returning anything, make sure to include the right return type in the function declaration
```
//bad
public getUser(id: string) {...}

//good
public getUser(id: string): User {
    ...
}

//with async/await
public async getUser(id: string): Promise<User> {
    ...
    return user;
}
```

## Classes
- Use `PascalCase` for class name
```
//bad
class userClass {....}

//good
class UserClass {
    ...
}
```

## Interfaces
- Use `PascalCase` for interface name and `camelCase` for members
```
//bad
interface user {
    Name: string;
    Age: number;
}

//good
interface User {
    name: string;
    age: number;
}
```

- Do not prefix `I` to an interface name
```
//bad
interface IUser {...}

//good
interface User {...}
```

## References
- Use `const` or `let` for all your references. Do not use `var`.
```
//bad
var a: number = 1;
var b: string = 'Hello';

//good
const a: number = 1;
let b: string = "Hello";
```
`var` is function scoped whereas `let` and `const` are block-scoped. Refering any variable as a `var` leaves it vulnerable to be reassigned through the function.


## Objects
- Use either interface or type declaration for objects. Do not use anonymous objects
```
//bad
let user = {
    name: "John Doe",
    age: 30
}

//good
let user: User = {
    name: "John Doe",
    age: 30
}

//where User is either a type
type User = {
    name: string;
    age: number;
}

//or an interface

interface User {
    name: string;
    age: number;
}
```

- Only use quote properties for invalid identifiers
```
// bad
const bad = {
  'foo': 3,
  'bar': 4,
  'data-blah': 5,
};

// good
const good = {
  foo: 3,
  bar: 4,
  'data-blah': 5,
};
```

- Do not call Object.prototype methods directly, such as `hasOwnProperty`, `propertyIsEnumerable` and `isPrototypeOf`
> Why? These methods may be shadowed by properties on the object in question - consider `{ hasOwnProperty: false }` - or, the object may be a null object `(Object.create(null))`.

```
// bad
console.log(object.hasOwnProperty(key));

// good
console.log(Object.prototype.hasOwnProperty.call(object, key));

// best
const has = Object.prototype.hasOwnProperty; // cache the lookup once, in module scope.
console.log(has.call(object, key));
/* or */
import has from 'has'; // https://www.npmjs.com/package/has
console.log(has(object, key));
```

- Prefer the object spread syntax over `Object.assign` to shallow-copy objects. Use the object rest parameter syntax to get a new object with certain properties omitted.
```
// very bad
const original = { a: 1, b: 2 };
const copy = Object.assign(original, { c: 3 }); // this mutates `original` ಠ_ಠ
delete copy.a; // so does this

// bad
const original = { a: 1, b: 2 };
const copy = Object.assign({}, original, { c: 3 }); // copy => { a: 1, b: 2, c: 3 }

// good
const original = { a: 1, b: 2 };
const copy = { ...original, c: 3 }; // copy => { a: 1, b: 2, c: 3 }
```

## Arrays
- Annotate arrays as `foos: Foo[]` instead of `foos: Array<Foo>`
> Why?: It's easier to read. It's used by the TypeScript team. Makes easier to know something is an array as the mind is trained to detect [].

## Strings
- Strings should be represented in double quotes ("") instead of single quotes ('')
- This includes all string representations like module imports, declaring a constant value etc

```
//bad
import Logger from '../services/logger';
const default: string = 'Some String';

//good
import Logger from "../services/logger";
const default: string = "Some String";
```
